import React from 'react';

import './Recipe.css';

const recipe = (props) => (
    <article className="Recipe" onClick={props.fetchRecipeData}>
        <h1>{props.title}</h1>
        <img src={props.photo_url} alt={props.title}/>
        Click to read more +
    </article>
);

export default recipe;
