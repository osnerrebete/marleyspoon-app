import React from 'react';
import axios from 'axios';
import './RecipeData.css';

class RecipeData extends React.Component {
    state = {
        recipe: null
    }

    componentDidUpdate() {
        if ((this.props.recipeId && !this.state.recipe) || (this.props.recipeId && this.props.recipeId !== this.state.recipe.id)) {
            axios.get('http://127.0.0.1:3000/recipes/' + this.props.recipeId)
                 .then(response => {
                     this.setState({
                                       recipe: response.data
                                   })
                 })
        }
    }

    render() {
        let recipe = <p>Please click on a recipe to see more details!</p>

        if (this.state.recipe) {
            recipe = (
                <div className='RecipeData'>
                    <h1>{this.state.recipe.title}</h1>
                    <img src={this.state.recipe.photo_url} alt={this.state.recipe.title}/>
                    <p>{this.state.recipe.description}</p>
                    <p>Tags: {this.state.recipe.tags.map(tag => tag.name).join(', ')}</p>
                    <p>Chefs: {this.state.recipe.chefs.map(chef => chef.name).join(', ')}</p>
                </div>
            )
        }

        return recipe;
    }
}

export default RecipeData;
