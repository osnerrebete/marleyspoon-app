import React, { Component } from 'react';
import axios from 'axios';
import Recipe from '../../components/Recipe/Recipe';
import RecipeData from '../../components/RecipeData/RecipeData';
import './FoodBlog.css';

class FoodBlog extends Component {
    state = {
        recipes: [],
        selectedRecipeId: null
    }

    componentDidMount() {
        axios.get('http://127.0.0.1:3000/recipes')
             .then(response => {
                 this.setState({
                                   recipes: response.data
                               })
             });
    }

    recipeSelectedHandler(recipeId) {
        this.setState({selectedRecipeId: recipeId});
    }

    render() {
        const recipes = this.state.recipes.map(recipe => {
            return <Recipe
                key={recipe.id}
                title={recipe.title}
                photoUrl={recipe.photo_url}
                fetchRecipeData={() => this.recipeSelectedHandler(recipe.id)}
            />
        });
        return (
            <div>
                <section className="FoodPosts">
                    {recipes}
                </section>
                <hr/>
                <section className="FoodPosts">
                    <h1>Selected recipe</h1>
                    <RecipeData recipeId={this.state.selectedRecipeId}/>
                </section>

            </div>
        );
    }
}

export default FoodBlog;
