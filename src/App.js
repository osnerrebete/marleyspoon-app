import './App.css';
import FoodBlog from './containers/FoodBlog/FoodBlog';

function App() {
  return (
      <div className="App">
          <FoodBlog />
      </div>
  );
}

export default App;
